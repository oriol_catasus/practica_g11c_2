json.array! @mix do |comment|
    json.id comment.id
    json.text comment.text
    json.escomment comment.escomment
    json.points comment.points
    json.created_at comment.created_at
    json.updated_at comment.updated_at
    json.user comment.user
    json.contribution comment.contribution
    json.contribution_title Contribution.find(comment.contribution).title
    json.user_name User.find(comment.user).user
end