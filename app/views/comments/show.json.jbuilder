json.array! @comments do |com|
  json.id com.id
  json.text com.text
  json.escomment com.escomment
  json.points com.points
  json.created_at com.created_at
  json.updated_at com.updated_at
  json.user com.user
  json.contribution com.contribution
  json.contribution_title Contribution.find(com.contribution).title
  json.user_name User.find(com.user).user
  json.num_replies Comment.where(comment_id: com.id).count
end