json.array! @reply do |reply|
    json.text comment.text
    json.user comment.user
    json.comment_id comment.comment_id
    json.created_at comment.created_at
    json.updated_at comment.updated_at
    json.points comment.points
    json.contribution comment.contribution
end