json.id @contribution.id
json.title @contribution.title
json.url @contribution.url
json.text @contribution.text
json.shorturl @contribution.shorturl
json.created_at @contribution.created_at
json.updated_at @contribution.updated_at
json.points @contribution.points
json.user_id @contribution.user_id
json.num_comments Comment.where(contribution: @contribution.id).count
json.user_name User.find(@contribution.user_id).user