json.id @user.id
json.user @user.user
json.karma @user.karma
json.about @user.about
json.created_at @user.created_at
json.updated_at @user.updated_at
json.email @user.email
json.authentication_token @user.authentication_token