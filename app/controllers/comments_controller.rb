class CommentsController < ApplicationController
  skip_before_action :verify_authenticity_token
  
  def index
    if params[:users_id] != nil
      if params[:voted] == nil || params[:voted] == "false"
        @mix = Comment.where(user: params[:users_id])
      elsif params[:voted] == "true"
        signin_apikey
        if params[:users_id].to_i != session[:user_id].to_i
          msg = "You can't see the voted comment from other users"
          respond_to do |format|
            format.html { redirect_to '/contributions' }
            format.json { render json: msg.to_json , status: 403, location: @newest }
          end
        else 
          @votesComment = Vote.where(tipus: "comment", idUser: params[:users_id])
          @mix = []
          @votesComment.each do |vote|
            @mix.push(Comment.find(vote.idType))
          end
        end
        @votesComment = Vote.where(tipus: "comment", idUser: params[:users_id])
        @mix = []
        @votesComment.each do |vote|
          @mix.push(Comment.find(vote.idType))
        end
      end
    else
      @mix = Comment.all.order("created_at DESC")
    end
  end
  
  def show
    @comment = Comment.find(params[:id])
    if params[:vote] != nil
      signin_apikey
      if session[:user_id].nil?
        respond_to do |format|
          @msg = 'Invalid user'
          format.json { render json: @msg.to_json, status: 403 }
        end
      else
        @msg = ""
        if params[:vote] == "true"
          if @comment.user == session[:user_id]
            respond_to do |format|
              @msg = "You cannot vote your own comment"
              format.json { render json: @msg.to_json, status: 403}
            end
            return
          end
          @vote = Vote.where(idType: params[:id], idUser: session[:user_id])
          if !@vote.empty?
            respond_to do |format|
              @msg = "You cannot vote the same comment twice"
              format.json { render json: @msg.to_json, status: 400}
            end
            return
          end
          @vote = Vote.new
          @vote.tipus = "comment"
          @vote.idType = params[:id]
          @vote.idUser = session[:user_id]
          @vote.save
          @comment.points += 1;
          @comment.save
          @msg = "Comment voted successfully"
        else
          @vote = Vote.find_by(tipus: "comment", idType: params[:id], idUser: session[:user_id])
          if @vote.nil?
            respond_to do |format|
              @msg = "You cannot unvote a comment which you has not been voted"
              format.json {render json: @msg.to_json, status: 400}
            end
            return
          end  
          @vote.destroy
          @comment.points -= 1;
          @comment.save
          @msg = "Comment unvoted successfully"
        end
        if params[:from] != nil
          if params[:from] == "contribution"
            respond_to do |format|
            format.html { redirect_to '/contribution_comments/'+@comment.contribution.to_s }
            end
          elsif params[:from] == "comments"
            respond_to do |format|
            format.html { redirect_to '/comments/'}
            end
          end
        else
          respond_to do |format|
            format.html { redirect_to '/contribution_comments/'+@comment.contribution.to_s }
            format.json { render json: @msg.to_json, status: 200 }
          end
        end
      end
    elsif params[:replies] == "true"
          @comments = Comment.where(comment_id: params[:id])
    else
      respond_to do |format|
        format.json { render json: @comment, status: 200 }
      end
    end
  end
  
  # PATCH/PUT /comments/1.json
  def update
    signin_apikey
    @comment = Comment.find(params[:id])
    if session[:user_id].to_i != @comment.user.to_i
      msg = "You can't edit a comment that is not yours"
       respond_to do |format|
        format.html { redirect_to '/comments' }
        format.json { render json: msg.to_json , status: 403, location: @contribution }
      end
    elsif params[:text].blank?
      respond_to do |format|
        msg = "Text cannot be blank"
        format.json { render json: msg.to_json , status: 400, location: @comment }
      end
    else
      @comment.text = params[:text]
      @comment.save
      respond_to do |format|
        format.json { render json: @comment.to_json, status: 200 }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    signin_apikey
    @comment = Comment.find(params[:id])
    if @user.nil? || @comment.user != @user.id
      respond_to do |format|
        @msg = "The user does not have permissions to delete this comment"
        format.json { render json: @msg.to_json, status: 403 }
      end
    else
      borrar_replies(@comment)
      respond_to do |format|
        format.html { redirect_to contributions_url, notice: 'Contribution was successfully destroyed.' }
        @msg = "Comment successfully deleted"
        format.json { render json: @msg.to_json, status: 200 }
      end
    end
  end

  private
    def signin_apikey
      if request.headers["X-API-KEY"] != nil
        puts "Header:"
        puts request.headers["X-API-KEY"]
        @user = User.where(authentication_token: request.headers["X-API-KEY"]).take
        session[:user_id] = @user.id
      end
    end
    
    def borrar_replies(comment)
      @coments = Comment.where(comment_id: comment.id)
      @coments.each do |com|
        borrar_replies(com)
      end
      Vote.where(tipus: 'comment').where(idType: comment.id).destroy_all
      comment.destroy
    end
end

