class ContributionCommentsController < ApplicationController
  skip_before_action :verify_authenticity_token
  
  def index
  end
  
  def show
    @contribution = Contribution.find(params[:id])
    if params[:vote] != nil
      if params[:vote] == "true"
        @vote = Vote.new
        @vote.tipus = "contribution"
        @vote.idType = @contribution.id
        @vote.idUser = session[:user_id]
        @vote.save
        @contribution.points += 1
        @contribution.save
      elsif params[:vote] == "false"
        @vote = Vote.find_by(tipus: "contribution", idType: params[:id], idUser: session[:user_id])
        @vote.destroy
        @contribution.points -= 1
        @contribution.save
      end
      respond_to do |format|
        format.html { redirect_to '/contribution_comments/'+@contribution.id.to_s }
      end
    end
    @comment = Comment.where(contribution: params[:id], escomment: true).order("points DESC")
  end
  
  def new
    @comment = Comment.new
  end
  
  def create
    signin_apikey
    if !session[:user_id].nil?
      @contribution = Contribution.find(params[:contribution])
      if @contribution.user_id.to_i == session[:user_id].to_i
        respond_to do |format|
          msg = 'A user cannot comment its own contribution'
          format.json {render json: msg.to_json, status: 403 }
        end
      else
        @comment = Comment.new(comment_params)
        if (@comment != nil)
          @comment.user = session[:user_id]
          @comment.escomment = true
          @comment.comment_id = nil
          respond_to do |format|
            if @comment.text.blank?
              format.html { redirect_to '/contribution_comments/' + @comment.contribution.to_s, alert: 'Please try again.' }
              msg = 'Text property cannot be blank'
              format.json { render json: msg.to_json, status: 400  }
            elsif @comment.save
              format.html { redirect_to '/contribution_comments/' + @comment.contribution.to_s }
              format.json { render json: @comment, status: 201 }
            else
              format.html { render :new }
              format.json { render json: @comment.errors, status: :unprocessable_entity }
            end
          end
        else
          respond_to do |format|
            format.html { redirect_to '/contribution_comments/' + @comment.contribution.to_s }
          end
        end
      end
    end
  end
  
  def comment_params
      params.permit(:text, :contribution, :user, :escomment, :comment_id)
  end
  
  private
    def signin_apikey
      if request.headers["X-API-KEY"] != nil
        puts "Header:"
        puts request.headers["X-API-KEY"]
        @user = User.where(authentication_token: request.headers["X-API-KEY"]).take
        session[:user_id] = @user.id
      end
    end
  
end
