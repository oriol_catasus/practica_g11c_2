class DeleteController < ApplicationController
    skip_before_action :verify_authenticity_token
  
    def index
      @contribution = Contribution.find(params[:cont_id])
    end
    
    def show
      @contribution = Contribution.find(params[:cont_id])
    end
    
  # DELETE /contributions/1
  # DELETE /contributions/1.json
  def destroy
    set_contribution
    if params[:d] == 'Yes'
      @coments = Comment.where(contribution: params[:id])
      @coments.each do |com|
        Reply.where(comment_id: com.id).destroy_all
        com.destroy
      end
      @contribution.destroy
    end
    respond_to do |format|
      format.html { redirect_to contributions_url, notice: 'Contribution was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
  
  def set_contribution
      @contribution = Contribution.find(params[:id])
  end
  
  def contribution_params
      params.require(:contribution).permit(:title, :url, :text, :cont_id, :d)
  end
end
