class UsersController < ApplicationController
  skip_before_action :verify_authenticity_token
  
  def index
    signin_apikey
    respond_to do |format|
      format.json { render json: @user, status: 200 }
    end
  end
  
  def show
    if params[:logout] != nil
      session.clear
      respond_to do |format|
        format.html { redirect_to '/contributions'}
      end
    else
      @user = User.find(params[:id])
    end
  end
  
  def update
    signin_apikey
    if (session[:user_id].to_s == params[:id].to_s)
      @user.update(user_params)
      respond_to do |format|
        format.html { redirect_to @user }
        format.json { render json: "Ok".to_json, status: 200 }
      end
    else
      respond_to do |format|
        format.json { render json: "L'usuari no té permisos per modificar aquest perfil".to_json, status: 403 }
      end
    end
  end
  
  def create
    @user = User.create
    @user.user = params[:name]
    @user.email = params[:email]
    @user.google_token = params[:google_token]
    @user.authentication_token = Digest::MD5.hexdigest @user.email
    @user.save
    respond_to do |format|
      format.json { render json: @user, status: 200 }
    end
  end
  
  private
    def signin_apikey
      if request.headers["X-API-KEY"] != nil
        puts "Header:"
        puts request.headers["X-API-KEY"]
        @user = User.where(authentication_token: request.headers["X-API-KEY"]).take
        session[:user_id] = @user.id
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.permit(:about, :email, :id, :username, :password)
    end
end
