class EditController < ApplicationController
   skip_before_action :verify_authenticity_token
  # PATCH/PUT /contributions/1
  # PATCH/PUT /contributions/1.json
  
  def index
    @contribution = Contribution.find(params[:cont_id])
  end
  
  def update
    set_contribution
    respond_to do |format|
      if contribution_params[:title].blank?
        format.html { redirect_to '/edit?cont_id=' + @contribution.id.to_s, alert: 'Please try again.' }
        format.json { render json: msg.to_json , status: 400, location: @contribution }
      elsif @contribution.update(contribution_params)
        format.html { redirect_to contributions_url, notice: 'Contribution was successfully updated.' }
        format.json { render json: msg.to_json , status: 200, location: @contribution }
      else
        format.html { render :edit }
        format.json { render json: msg.to_json , status: 400, location: @contribution }
      end
    end
  end
  
  private
  
  def set_contribution
      @contribution = Contribution.find(params[:id])
  end
  
  def contribution_params
      params.permit(:title, :url, :text, :cont_id)
  end
end
