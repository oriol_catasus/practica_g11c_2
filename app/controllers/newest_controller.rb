class NewestController < ApplicationController
  before_action :set_contribution, only: [:show, :edit, :update, :destroy]

  # GET /contributions
  # GET /contributions.json
  def index
    if params[:user_id] != nil
      if params[:voted] == nil || params[:voted] == "false"
        @contributions = Contribution.where(user_id: params[:user_id])
      elsif params[:voted] == "true"
        signin_apikey
        if params[:user_id].to_i != session[:user_id].to_i
          msg = "You can't see the voted submissions from other users"
          respond_to do |format|
            format.html { redirect_to '/contributions' }
            format.json { render json: msg.to_json , status: 403, location: @newest }
          end
        else 
          @votes = Vote.where(tipus: "contribution", idUser: params[:user_id])
          @contributions = []
          @votes.each do |vote|
            @contributions.push(Contribution.find(vote.idType))
          end
        end
      end  
    else
      @contributions = Contribution.all.order("created_at DESC")
    end
  end

  # GET /contributions/1
  # GET /contributions/1.json
  def show
    if params[:vote] != nil
      if params[:vote] == "true"
        @vote = Vote.new
        @vote.tipus = "contribution"
        @vote.idType = @contribution.id
        @vote.idUser = session[:user_id]
        @vote.save
        @contribution.points += 1
        @contribution.save
      elsif params[:vote] == "false"
        @vote = Vote.find_by(tipus: "contribution", idType: params[:id], idUser: session[:user_id])
        @vote.destroy
        @contribution.points -= 1
        @contribution.save
      end
      respond_to do |format|
        format.html { redirect_to '/newest' }
      end
    elsif params[:url_search] != nil
      @contributions =  Contribution.find(tipus: "contribution", urlshort: "url_search")
      @contributions.each do |format|
        format.html { redirect_to '/contributions' }
      end
    end
  end

  # GET /contributions/new
  def new
    @contribution = Contribution.new
  end

  # GET /contributions/1/edit
  def edit
  end

  # POST /contributions
  # POST /contributions.json
  def create
    @contribution = Contribution.new(contribution_params)

    respond_to do |format|
      if @contribution.save
        format.html { redirect_to @contribution, notice: 'Contribution was successfully created.' }
        format.json { render :show, status: :created, location: @contribution }
      else
        format.html { render :new }
        format.json { render json: @contribution.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contributions/1
  # PATCH/PUT /contributions/1.json
  def update
    respond_to do |format|
      if @contribution.update(contribution_params)
        format.html { redirect_to @contribution, notice: 'Contribution was successfully updated.' }
        format.json { render :show, status: :ok, location: @contribution }
      else
        format.html { render :edit }
        format.json { render json: @contribution.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contributions/1
  # DELETE /contributions/1.json
  def destroy
    @contribution.destroy
    respond_to do |format|
      format.html { redirect_to contributions_url, notice: 'Contribution was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def signin_apikey
      if request.headers["X-API-KEY"] != nil
        puts "Header:"
        puts request.headers["X-API-KEY"]
        @user = User.where(authentication_token: request.headers["X-API-KEY"]).take
        session[:user_id] = @user.id
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_contribution
      @contribution = Contribution.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contribution_params
      params.require(:contribution).permit(:title, :url, :text, :shorturl)
    end
end
