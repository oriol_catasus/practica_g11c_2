class RepliesController < ApplicationController
  skip_before_action :verify_authenticity_token
  
  def new
  end
  
  def create
    signin_apikey
    if session[:user_id].nil?
      respond_to do |format|
        msg = "User must be registered"
        format.json { render json: msg.to_json, status: 403  }
      end
    else
      @reply = Comment.new(reply_params)
      @reply.escomment = false
      @reply.user = session[:user_id]
      @comment = Comment.find(@reply.comment_id)
      if @reply != nil
        respond_to do |format|
          if @reply.text.blank?
            format.html { redirect_to '/replies/' + @comment.id.to_s, alert: 'Please try again.' }
            msg = "Text property cannot be blank"
            format.json { render json: msg.to_json, status: 400 }
          elsif @reply.save
            format.html { redirect_to '/contribution_comments/' + @comment.contribution.to_s }
            format.json { render json: @reply, status: :created }
          else
            format.html { render :new }
            format.json { render json: @reply.errors, status: :unprocessable_entity }
          end
        end
      else
        respond_to do |format|
          format.html { redirect_to '/contribution_comments/' + @comment.contribution.to_s }
        end
      end
    end
  end
  
  
  def show
    if params[:vote] != nil
      @reply = Comment.find(params[:id])
      @comment = Comment.find(@reply.comment_id)
      if params[:vote] == "true"
        @vote = Vote.new
        @vote.tipus = "comment"
        @vote.idType = params[:id]
        @vote.idUser = session[:user_id]
        @vote.save
        @reply.points += 1
        @reply.save
      elsif params[:vote] != nil &&  params[:vote] == "false"
        @vote = Vote.find_by(tipus: "reply", idType: params[:id], idUser: session[:user_id])
        @vote.destroy
        @reply.points -= 1
        @reply.save
      end
      if params[:from] != nil
        if params[:from] == "contribution"
          respond_to do |format|
            format.html { redirect_to '/contribution_comments/'+@comment.contribution.to_s }
          end
        elsif params[:from] == "comments"
          respond_to do |format|
          format.html { redirect_to '/comments/'}
          end
        end
      else
        respond_to do |format|
          format.html { redirect_to '/contribution_comments/'+@comment.contribution.to_s }
        end
      end
    else
      @comment = Comment.find(params[:id])
    end
  end
  
  private
    def reply_params
        params.require(:reply).permit(:text, :comment_id, :user, :contribution)
    end
    
    def signin_apikey
      if request.headers["X-API-KEY"] != nil
        puts "Header:"
        puts request.headers["X-API-KEY"]
        @user = User.where(authentication_token: request.headers["X-API-KEY"]).take
        session[:user_id] = @user.id
      end
    end
end
