class ContributionsController < ApplicationController
  before_action :set_contribution, only: [:show, :edit, :update, :destroy, :vote]
  skip_before_action :verify_authenticity_token

  # GET /contributions
  # GET /contributions.json
  def index
    if params[:user_id] != nil
      if params[:voted] == nil || params[:voted] == "false"
        @contributions = Contribution.where(user_id: params[:user_id])
      elsif params[:voted] == "true"
        @votes = Vote.where(tipus: "contribution", idUser: params[:user_id])
        @contributions = []
        @votes.each do |vote|
          @contributions.push(Contribution.find(vote.idType))
        end
      end
    elsif params[:url_search] != nil
        @contributio = Contribution.where(shorturl: params[:url_search])
        @contributions = []
        @contributio.each do |contr|
          @contributions.push(contr)
        end
    else
      @contributions = Contribution.all.order("points DESC")
    end
  end

  # GET /contributions/1
  # GET /contributions/1.json
  def show
    signin_apikey
    if params[:vote] != nil
      if @contribution.user_id == session[:user_id]
        msg = "You can't vote your contribution"
        respond_to do |format|
          format.html { redirect_to '/contributions' }
          format.json { render json: msg.to_json , status: 403, location: @contribution }
        end
      else
        if params[:vote] == "true"
          @vote = Vote.find_by(tipus: "contribution", idType: params[:id], idUser: session[:user_id])
          if @vote != nil
            msg = "You have alredy voted this contribution"
            respond_to do |format|
              format.html { redirect_to '/contributions' }
              format.json { render json: msg.to_json , status: 400, location: @contribution }
            end
          else
            @vote = Vote.new
            @vote.tipus = "contribution"
            @vote.idType = params[:id]
            @vote.idUser = session[:user_id]
            @vote.save
            @contribution.points += 1
            @contribution.save
            msg = "The contribution has been voted"
            respond_to do |format|
              format.html { redirect_to '/contributions' }
              format.json { render json: msg.to_json , status: 200, location: @contribution }
            end
          end
        elsif params[:vote] == "false"
          @vote = Vote.find_by(tipus: "contribution", idType: params[:id], idUser: session[:user_id])
          if @vote.nil?
            msg = "You have alredy unvoted this contribution"
            respond_to do |format|
            format.html { redirect_to '/contributions' }
            format.json { render json: msg.to_json , status: 400, location: @contribution }
          end
          else
            @vote.destroy
            @contribution.points -= 1
            @contribution.save
            msg = "The contribution has been unvoted"
            respond_to do |format|
              format.html { redirect_to '/contributions' }
              format.json { render json: msg.to_json , status: 200, location: @contribution }
            end
          end
        end
      end
    end
  end

  # GET /contributions/new
  def new
    @contribution = Contribution.new
  end

  # GET /contributions/1/edit
  def edit
  end

  # POST /contributions
  # POST /contributions.json
  def create
    signin_apikey
    if session[:user_id] != nil
      @contribution = Contribution.new(contribution_params)
      @contribution.user_id = session[:user_id]
      if !@contribution.url.blank?
        @contribution.url = @contribution.url.sub "https://", ""
        @contribution.url = @contribution.url.sub "www.", ""
        @urlsplit = @contribution.url.split("/")
            @urlsplit.each do |suburl|
              if suburl.include?(".")
                @contribution.shorturl = suburl
              end
            end
      end
      @contribution2 = Contribution.find_by url: @contribution.url
      if (@contribution2 == nil || @contribution.url.blank?) 
        respond_to do |format|
          if @contribution.title.blank?
            format.html { redirect_to new_contribution_path, alert: 'Please try again.' }
            format.json { render json: "Title property cannot be blank".to_json, status: 400 }
          elsif (!@contribution.url.blank? and !@contribution.text.blank?)
            format.html { redirect_to new_contribution_path, alert: "Submissions can't have both urls and text, so you need to pick one. 
            If you keep the url, you can always post your text as a comment in the thread." }
            format.json { render json: "Submissions can't have both urls and text".to_json, status: 400 }
          elsif @contribution.save
              format.html { redirect_to '/newest', notice: 'Contribution was successfully created.' }
              format.json { render json: @contribution, status: 201 }
          else
            format.html { render :new }
            format.json { render json: @contribution.errors, status: :unprocessable_entity }
          end
        end
      else
        respond_to do |format|
          format.html { redirect_to '/contributions/' + @contribution2.id.to_s }
          msg = "The URL already exists"
          format.json { render json: msg.to_json , status: 400, location: @contribution }
        end
      end
    else
      respond_to do |format|
          format.json { render json: @contribution.errors, status: 400 }
      end
    end
  end

  # PATCH/PUT /contributions/1
  # PATCH/PUT /contributions/1.json
  def update
    signin_apikey
    if session[:user_id].to_i != @contribution.user_id.to_i
      msg = "You can't edit a contribution that is not yours"
       respond_to do |format|
        format.html { redirect_to '/contributions' }
        format.json { render json: msg.to_json , status: 403, location: @contribution }
      end
    else  
      set_contribution
      respond_to do |format|
        if contribution_params[:title].blank?
          format.html { redirect_to '/edit?cont_id=' + @contribution.id.to_s, alert: 'Please try again.' }
          msg = "The title is blank"
          format.json { render json: msg.to_json , status: 400, location: @contribution }
        elsif (!@contribution.url.blank? and !contribution_params[:text].blank?)
              format.html { redirect_to new_contribution_path, alert: "Submissions can't have both urls and text, so you need to pick one. 
              If you keep the url, you can always post your text as a comment in the thread." }
              msg = "A contribution with an URL can't have a text"
              format.json { render json: msg.to_json , status: 400, location: @contribution }
        elsif @contribution.update(contribution_params)
          format.html { redirect_to contributions_url, notice: 'Contribution was successfully updated.' }
          format.json { render :show, status: :ok, location: @contribution }
        else
          format.html { render :edit }
          format.json { render json: @contribution.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /contributions/1
  # DELETE /contributions/1.json
  def destroy
    signin_apikey
    set_contribution
    if @user.nil? || @contribution.user_id != @user.id
      respond_to do |format|
        @msg = "The user does not have permissions to delete this contribution"
        format.json { render json: @msg.to_json, status: 403 }
      end
    else
      @coments = Comment.where(contribution: params[:id])
      @coments.each do |com|
        Vote.where(tipus: 'comment').where(idType: com.id).destroy_all
        com.destroy
      end
      Vote.where(tipus: 'contribution').where(idType: params[:id]).destroy_all
      @contribution.destroy
      respond_to do |format|
        format.html { redirect_to contributions_url, notice: 'Contribution was successfully destroyed.' }
        @msg = "Contribution successfully deleted"
        format.json { render json: @msg.to_json, status: 200 }
      end
    end
  end

  # POST /contributions/1/vote
  def votes
    format.html { redirect_to contributions_url, notice: 'Contribution was successfully updated.' }
  end
    

  private
    def signin_apikey
      if request.headers["X-API-KEY"] != nil
        puts "Header:"
        puts request.headers["X-API-KEY"]
        @user = User.where(authentication_token: request.headers["X-API-KEY"]).take
        session[:user_id] = @user.id
      end
    end
  
    # Use callbacks to share common setup or constraints between actions.
    def set_contribution
      @contribution = Contribution.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contribution_params
      params.require(:contribution).permit(:title, :url, :text, :urlshort, :d)
    end
end
