class AddUserToContribution < ActiveRecord::Migration[6.0]
  def change
    add_column :contributions, :user_id, :integer
    add_foreign_key :contributions, :users
  end
end
