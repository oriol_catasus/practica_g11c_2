class CreateUser < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :user
      t.string :password
      t.integer :karma, null: false, default: 1
      t.string :about
      t.timestamps null: false
    end
  end
end

