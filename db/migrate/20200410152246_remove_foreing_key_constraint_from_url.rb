class RemoveForeingKeyConstraintFromUrl < ActiveRecord::Migration[6.0]
  def change
      remove_index :contributions, :url
  end
end
