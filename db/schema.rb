# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_05_154656) do

  create_table "comments", force: :cascade do |t|
    t.string "text"
    t.boolean "escomment"
    t.integer "points", default: 1
    t.integer "user", null: false
    t.integer "contribution", null: false
    t.integer "comment_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "contributions", force: :cascade do |t|
    t.string "title"
    t.string "url"
    t.string "text"
    t.string "shorturl"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "points", default: 1
    t.integer "user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "user"
    t.string "password"
    t.integer "karma", default: 1, null: false
    t.string "about"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "google_token"
    t.string "google_refresh_token"
    t.string "email"
    t.string "authentication_token"
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true
  end

  create_table "votes", force: :cascade do |t|
    t.string "tipus", null: false
    t.integer "idType", null: false
    t.integer "idUser", null: false
    t.datetime "created_at"
    t.index ["tipus", "idType", "idUser"], name: "index_vote_on_type_idType_idUser", unique: true
  end

  add_foreign_key "contributions", "users"
end
