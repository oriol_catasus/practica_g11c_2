Rails.application.routes.draw do
  
  resources :contributions
  resources :newest
  resources :ask
  resources :comments
  resources :contribution_comments
  resources :users
  resources :formatdoc
  resources :replies
  resources :edit
  resources :delete
  root 'contributions#index'
  
  post "users/login", action: :login, controller: 'users'
  post "contribution_comments/:id:", action: :create, controller: 'contribution_comments'
  #post "contributions/new", action: :login, controller: 'users'
  
  # Routes for Google authentication
  get "auth/:provider/callback", to: "sessions#googleAuth"
  get "auth/failure", to: redirect("/")
end